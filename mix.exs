defmodule Tentacat.Mixfile do
  use Mix.Project

  @description """
    Simple Elixir wrapper for the Bitbucket API
  """

  def project do
    [
      app: :bitbucket_api,
      version: "0.8.0",
      elixir: "~> 1.6",
      name: "BitbucketApi",
      description: @description,
      package: package(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [coveralls: :test, "coveralls.detail": :test, "coveralls.post": :test],
      deps: deps()
    ]
  end

  def application do
    [applications: [:httpoison, :exjsx]]
  end

  defp deps do
    [
      {:httpoison, "~> 0.13"},
      {:exjsx, "~> 3.2"},
      {:ex_doc, "~> 0.11.4", only: :docs},
      {:inch_ex, "~> 0.5", only: :docs},
      {:excoveralls, "~> 0.5", only: :test},
      {:exvcr, "~> 0.6", only: :test},
      {:meck, "~> 0.8", only: :test}
    ]
  end

  defp package do
    [
      maintainers: ["Jakob Dam Jensen"],
      licenses: ["MIT"],
      links: %{"Bitbicket" => "https://github.com/npa_io/bitbucket_api"}
    ]
  end
end
