defmodule BitbucketApi.Users do
  import BitbucketApi
  alias BitbucketApi.Client

  def show(username, client \\ %Client{}, params \\ [], options \\ []) do
    get "users/#{username}", client, params, options
  end
end
