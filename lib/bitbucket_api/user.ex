defmodule BitbucketApi.User do
  import BitbucketApi
  alias BitbucketApi.Client

  def user(client \\ %Client{}, params \\ [], options \\ []) do
    get "user", client, params, options
  end

  def emails(client \\ %Client{}, params \\ [], options \\ []) do
    get "user/emails", client, params, options
  end
end
