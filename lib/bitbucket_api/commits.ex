defmodule BitbucketApi.Commits do
  import BitbucketApi
  alias BitbucketApi.Client
  @moduledoc """
  The Commits API allows
  """

  @spec list(binary, binary, Client.t) :: BitbucketApi.response
  def list(username, repo_slug, client \\ %Client{}, params \\ [], options \\ []) do
    get "repositories/#{username}/#{repo_slug}/commits/", client, params, options
  end

  @spec show(binary, binary, binary, Client.t) :: BitbucketApi.response
  def show(username, repo_slug, commit_hash, client \\ %Client{}, params \\ [], options \\ []) do
    get "repositories/#{username}/#{repo_slug}/commit/#{commit_hash}", client, params, options
  end
end

