defmodule BitbucketApi.Hooks do
  import BitbucketApi
  alias BitbucketApi.Client

  def create(username, repo_slug, body, client \\ %Client{}) do
    post("repositories/#{username}/#{repo_slug}/hooks", client, body)
  end

  def update(username, repo_slug, webhook_uuid, body, client \\ %Client{}) do
    put("repositories/#{username}/#{repo_slug}/hooks/#{webhook_uuid}", client, body)
  end

  def delete(username, repo_slug, webhook_uuid, client \\ %Client{}) do
    delete("repositories/#{username}/#{repo_slug}/hooks/#{webhook_uuid}", client)
  end
end
