defmodule BitbucketApi.Repositories do
  import BitbucketApi
  alias BitbucketApi.Client
  @moduledoc """
  The Repository Webhooks API allows repository admins to manage the post-receive hooks for a repository.
  """

  @doc """
  List users Repositories.

  ## Example

      BitbucketApi.Repositories.list_users("steve", client)

  More info at: https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D
  """
  @spec list_users(binary, Client.t) :: BitbucketApi.response
  def list_users(_username, client \\ %Client{}, params \\ [], options \\ []) do
    get "repositories", client, params, options
  end

  def create(username, repo_slug, body, client \\ %Client{}) do
    post "repositories/#{username}/#{repo_slug}", client, body
  end
end
