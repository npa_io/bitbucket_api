defmodule BitbucketApi.PullRequests do
  import BitbucketApi
  alias BitbucketApi.Client
  alias BitbucketApi.LegacyClient
  @moduledoc """
  The Comments Webhooks API allows
  """

  @spec list(binary, binary, Client.t) :: BitbucketApi.response
  def list(username, repo_slug, client \\ %Client{}, params \\ [], options \\ []) do
    get "repositories/#{username}/#{repo_slug}/pullrequests", client, params, options
  end
end

