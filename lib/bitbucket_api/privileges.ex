defmodule BitbucketApi.Privileges do
  import BitbucketApi
  alias BitbucketApi.Client
  alias BitbucketApi.LegacyClient
  @moduledoc """
  The Issues Webhooks API allows
  """

  @doc """
  List issues in Repository.

  ## Example

      BitbucketApi.Repositories.list("steve", "todo_repo", client)

  More info at: https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/issues
  """
  @spec list(binary, binary, Client.t) :: BitbucketApi.response
  def list(username, repo_slug, client \\ %LegacyClient{}, params \\ [], options \\ []) do
    get "privileges/#{username}/#{repo_slug}", client, params, options
  end
end
