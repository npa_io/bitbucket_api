defmodule BitbucketApi.Issues do
  import BitbucketApi
  alias BitbucketApi.Client

  @moduledoc """
  The Issues Webhooks API allows
  """

  @doc """
  List issues in Repository.

  ## Example

      BitbucketApi.Repositories.list("steve", "todo_repo", client)

  More info at: https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/issues
  """
  @spec list(binary, binary, Client.t()) :: BitbucketApi.response()
  def list(username, repo_slug, client \\ %Client{}, params \\ [], options \\ []) do
    get("repositories/#{username}/#{repo_slug}/issues", client, params, options)
  end

  @spec create(binary, binary, binary, Client.t()) :: BitbucketApi.response()
  def create(username, repo_slug, body, client \\ %Client{}) do
    post("repositories/#{username}/#{repo_slug}/issues", client, body)
  end

  @spec delete(binary, binary, binary, Client.t()) :: BitbucketApi.response()
  def delete(username, repo_slug, issue_id, client \\ %Client{}) do
    delete("repositories/#{username}/#{repo_slug}/issues/#{issue_id}", client)
  end

  @spec update(binary, binary, binary, binary, Client.t()) :: BitbucketApi.response()
  def update(username, repo_slug, issue_id, body, client) do
    put("repositories/#{username}/#{repo_slug}/issues/#{issue_id}", client, body)
  end
end
