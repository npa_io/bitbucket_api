defmodule BitbucketApi.Comments do
  import BitbucketApi
  alias BitbucketApi.Client

  @moduledoc """
  """

  @spec update(binary, binary, binary, binary, binary, Client.t()) :: BitbucketApi.response()
  def update(username, repo_slug, issue_id, comment_id, content, client \\ %Client{}) do
    put("repositories/#{username}/#{repo_slug}/issues/#{issue_id}/comments", client, %{
      content: content
    })
  end

  @spec create(binary, binary, binary, binary, Client.t()) :: BitbucketApi.response()
  def create(username, repo_slug, issue_id, content, client \\ %Client{}) do
    post("repositories/#{username}/#{repo_slug}/issues/#{issue_id}/comments", client, %{
      content: %{raw: content}
    })
  end

  @spec list(binary, binary, Client.t()) :: BitbucketApi.response()
  def list(username, repo_slug, issue_id, client \\ %Client{}, params \\ [], options \\ []) do
    get(
      "repositories/#{username}/#{repo_slug}/issues/#{issue_id}/comments",
      client,
      params,
      options
    )
  end

  @spec delete(binary, binary, binary, binary, Client.t()) :: BitbucketApi.response()
  def delete(username, repo_slug, issue_id, comment_id, client \\ %Client{}) do
    delete(
      "repositories/#{username}/#{repo_slug}/issues/#{issue_id}/comments/#{comment_id}",
      client
    )
  end
end
