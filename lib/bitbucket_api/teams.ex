defmodule BitbucketApi.Teams do
  import BitbucketApi
  alias BitbucketApi.Client

  def show(teamname, client \\ %Client{}, params \\ [], options \\ []) do
    get "teams/#{teamname}", client, params, options
  end
end
