defmodule BitbucketApi.Components do
  import BitbucketApi
  alias BitbucketApi.Client

  @moduledoc """
  The Components Webhooks API allows
  """

  @doc """
  List components in Repository.

  More info at: https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/components
  """
  @spec list(binary, binary, Client.t()) :: BitbucketApi.response()
  def list(username, repo_slug, client \\ %Client{}, params \\ [], options \\ []) do
    get("repositories/#{username}/#{repo_slug}/components", client, params, options)
  end
end
